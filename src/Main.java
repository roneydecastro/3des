import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Main {
	
	static String KEY_PATH = "key.txt";
	
	public static void main(String[] args) {
		
		EncriptaDecripta3DES des3 = new EncriptaDecripta3DES();
		String type = args[0];
		String fileName = args[1];
		String outFile = args[2];
		Calendar cal = Calendar.getInstance();
	    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	    System.out.println( sdf.format(cal.getTime()) );
	    
		switch (type) {
		case "-e":
			try {
				des3.encrypt(KEY_PATH, fileName, outFile);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case "-d":
			try {
				des3.decrypt(fileName, outFile);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		}
		cal = Calendar.getInstance();
	    System.out.println( sdf.format(cal.getTime()) );
	}

}
