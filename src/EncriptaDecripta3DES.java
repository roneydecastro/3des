import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;


public class EncriptaDecripta3DES {
	/**
	   * The program. The first argument must be -e, -d, or -g to encrypt,
	   * decrypt, or generate a key. The second argument is the name of a file
	   * from which the key is read or to which it is written for -g. The -e and
	   * -d arguments cause the program to read from standard input and encrypt or
	   * decrypt to standard output.
	   */
	  public SecretKey key;
	  byte[] buf = new byte[1024];



	  public void generateSecreteKey(String filePath){
		  try {
		      try {
		        Cipher c = Cipher.getInstance("DESede");
		      } catch (Exception e) {
		        System.err.println("Installing SunJCE provider.");
		        Provider sunjce = new com.sun.crypto.provider.SunJCE();
		        Security.addProvider(sunjce);
		      }

		      // This is where we'll read the key from or write it to
		      File keyfile = new File(filePath);

		      // Now check the first arg to see what we're going to do
		        System.out.print("Generating key. This may take some time...");
		        System.out.flush();
		        key = generateKey();
		        writeKey(key, keyfile);
		        System.out.println("done.");
		        System.out.println("Secret key written to " + filePath
		            + ". Protect that file carefully!");

		    } catch (Exception e) {
		      System.err.println(e);
		      System.err.println("Usage: java " + EncriptaDecripta3DES.class.getName()
		          + " -d|-e|-g <keyfile>");
		    }
	  }
	  
	  public void getSecreteKey(String filePath){
		    
		    try {
		        Cipher c = Cipher.getInstance("DESede");
		    } catch (Exception e) {
		        System.err.println("Installing SunJCE provider.");
		        Provider sunjce = new com.sun.crypto.provider.SunJCE();
		        Security.addProvider(sunjce);
		    }

		  
		   //Delimiter used in CSV file
			try {
				File file = new File(filePath);
				BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
				
				byte[] buff=new byte[(int) file.length()];
				for (int i=0; i < file.length(); i++) {
				       buff[i]=(byte)bis.read();
				 }
			
				key = readKey(buff);

				} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				} catch (InvalidKeyException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvalidKeySpecException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	  }


	  /** Generate a secret TripleDES encryption/decryption key */
	  public SecretKey generateKey() throws NoSuchAlgorithmException {
	    // Get a key generator for Triple DES (a.k.a DESede)
	    KeyGenerator keygen = KeyGenerator.getInstance("DESede");
	    // Use it to generate a key
	    return keygen.generateKey();
	  }

	  /** Save the specified TripleDES SecretKey to the specified file */
	  public static void writeKey(SecretKey key, File f) throws IOException,
	      NoSuchAlgorithmException, InvalidKeySpecException {
	    // Convert the secret key to an array of bytes like this
	    SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DESede");
	    DESedeKeySpec keyspec = (DESedeKeySpec) keyfactory.getKeySpec(key,
	        DESedeKeySpec.class);
	    byte[] rawkey = keyspec.getKey();

	    // Write the raw key to the file
	    FileOutputStream out = new FileOutputStream(f);
	    out.write(rawkey);
	    out.close();
	  }

	  /** Read a TripleDES secret key from the specified file */
	  public SecretKey readKey(byte[] rawkey) throws IOException,
	      NoSuchAlgorithmException, InvalidKeyException,
	      InvalidKeySpecException {
	    // Read the raw bytes from the keyfile

	    // Convert the raw bytes to a secret key like this
	    DESedeKeySpec keyspec = new DESedeKeySpec(rawkey);
	    SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DESede");
	    SecretKey key = keyfactory.generateSecret(keyspec);
	    return key;
	  }

	  /**
	   * Use the specified TripleDES key to encrypt bytes from the input stream
	   * and write them to the output stream. This method uses CipherOutputStream
	   * to perform the encryption and write bytes at the same time.
	   */
	  public void encrypt(String filePathKey,String inFile, String outSendFile)  throws Exception{
		String outFile = outSendFile;
	    // Create and initialize the encryption engine
		OutputStream out = null;
		InputStream in = null;
			try {
				out = new FileOutputStream(outFile);
				in = new FileInputStream(inFile);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}  
		generateSecreteKey(filePathKey);
	    try{
			Cipher cipher = Cipher.getInstance("DESede");
		    cipher.init(Cipher.ENCRYPT_MODE, key);
	
		    // Create a special output stream to do the work for us
		    CipherOutputStream cos = new CipherOutputStream(out, cipher);
	
		    // Read from the input and write to the encrypting output stream
		    byte[] buffer = new byte[1024];
		    int bytesRead;
		    while ((bytesRead = in.read(buffer)) != -1) {
		      cos.write(buffer, 0, bytesRead);
		    }
		    cos.close();
	
		    // For extra security, don't leave any plaintext hanging around memory.
		    //java.util.Arrays.fill(buffer, (byte) 0);
	    }catch(NoSuchAlgorithmException e){
	    	System.out.println(e);
	    } catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
	    	System.out.println(e);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
	    	System.out.println(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
	    	System.out.println(e);
		}
	    
	   /* File[] files = new File[2];
		files[0] = new File(filePathKey);
		files[1] = new File(outFile);
		File mergedFile = new File(outSendFile);
		mergeFiles(files, mergedFile);*/

	  }

	  /**
	   * Use the specified TripleDES key to decrypt bytes ready from the input
	   * stream and write them to the output stream. This method uses uses Cipher
	   * directly to show how it can be done without CipherInputStream and
	   * CipherOutputStream.
	   */
	  public void decrypt(String inFile, String outFile){
		System.out.println("Decrypt Start");  
		OutputStream out = null;
		InputStream in = null;
		try {
			out = new FileOutputStream(outFile);
			in = new FileInputStream(inFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		getSecreteKey("key.txt");
		try{
	    // Create and initialize the decryption engine
	    Cipher cipher = Cipher.getInstance("DESede");
	    cipher.init(Cipher.DECRYPT_MODE, key);
	  
		decrypt(in,out,cipher);

	    //out.write(cipher.update(textoencriptado));
	    //out.write(cipher.doFinal());
	    
	    in.close();
	    System.out.println("Decrypt Done");
		}catch(NoSuchAlgorithmException e){
	    	System.out.println(e);
	    } catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
	    	System.out.println(e);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
	    	System.out.println(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
	    	System.out.println(e);
		}
	  }
	  
	  public void decrypt(InputStream in, OutputStream out, Cipher dcipher)
		{
			try
			{
				in = new CipherInputStream(in, dcipher);
				int numRead = 0;
				while ((numRead = in.read(buf)) >= 0)
				{
					out.write(buf, 0, numRead);
				}
			    out.flush();
				out.close();
				
			}
			catch (java.io.IOException e)
			{
			}
		}
	  
	  public static void mergeFiles(File[] files, File mergedFile) {
			 
          OutputStream out = null;
			try {
				out = new FileOutputStream(mergedFile);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}

			for (File f : files) {
				System.out.println("merging: " + f.getName());
				FileInputStream fis;
				try {
					fis = new FileInputStream(f);
		            InputStream in = new FileInputStream(f);
	 
					byte[] buf = new byte[1024];
			        int len;
			        while ((len = in.read(buf)) > 0){
			                out.write(buf, 0, len);
			        }
			        in.close();
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	 
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	 
		}
}